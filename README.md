# **Chat Censor** 
A simple, lightweight chat censor/profanity filter plugin for spigot. This plugin loads specified text files containing a word bank of bad words to use from and allows to censor them upon the player chat event. There are 3 censoring modes: none, mild, and full. The pre-defined word banks are provided thanks to this [**List of Dirty, Naughty, Obscene, and Otherwise Bad Words**](https://github.com/LDNOOBW/List-of-Dirty-Naughty-Obscene-and-Otherwise-Bad-Words/blob/master/README.md).
![Main Display](wiki/img/Capture.PNG)
```
Mode: NONE
 Output: this is an example: badass motherfucker
Mode: MILD
 Output: this is an example: bada*s motherf**ker
Mode: FULL
 Output: this is an example: bad*** mother****er
```
### **Demo** ###
Download the latest jar, and run it to enter the demo console `java -jar chatsensor.jar`

## **Commands** ##
`[ ]` Required argument

`< >` Optional argument and is for admin/mod players

| Command| Description |
| ------------- | :------------------------------ |
| `/censor`                        | Displays censor information. |
| `/censor mode [mode] <player>`   | Changes censor mode          |
|`/censor check <player>`          | List players' censor mode    |

## **Permissions** ##
| Permission| Description                    |
| ------------- | ------------------------------ |
| `chatsensor.admin`      | Access to admin privileges  |
| `chatsensor.monitor`      | Access to monitor privileges. Allows to switch between modes and receive alerts when player has sworn   |
| `chatsensor.full`      | Access to full censoring mode   |
| `chatsensor.mild`      | Access to mild censoring mode   |
| `chatsensor.none`      | Access to no censoring mode  |

## **Configuration.yml**
```
#!yaml

files: # The files containing the words to censor
- custom
- languages/en
messages:
  no-permission: ' §8(§l§e*§r§8)§r You do not have permission'
settings:
  enabled: true # Global censoring 
  censor: '*' 
  default-mode: MILD # [NONE, MILD, FULL]
```

# **API**
|Function/Variable| Description|
|---: | :--- |
|`Censor#ENABLED`   | A `Boolean` Status of censor system  |
|`Censor#DEFAULT_MODE`   | The default censoring mode    |
|`Censor#PLAYER_CACHE`   | A `Map` to store players' mode setting |
|`Censor#LOADED_FILES`   | A `List` of file names that's been loaded |
|`Censor#MASK`   | A `String` representing the censor Char |
|`Censor#BAD_WORDS`   | A `List` of bad words |
|`Censor#LETTER_PATTERNS`   | A `Map` of letters and their alternatives |
|`Censor#censorText(string, mode)`   | Transforms text to a censored text depending on mode.   |
|`Censor#filterBadwords(string, mode)`   | Checks specified string and replaces words containing from the `BAD_WORDS` list and censors them by the mode |
|`Censor#loadDictionary(directory: File, files: List, mode)`   | Loads a list of specified files and populates the `BAD_WORDS` list |