package io.bitbucket.sg007.chatcensor

import io.bitbucket.sg007.chatcensor.utils.Censor
import io.bitbucket.sg007.chatcensor.utils.Mode
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import org.bukkit.event.player.PlayerLoginEvent

class CensorListener : Listener {
    @EventHandler
    fun onPlayerChat(event: AsyncPlayerChatEvent) {
        val player = event.player
        val mode = if (Censor.GLOBAL_CENSOR) Censor.SERVER_MODE else player.censor
        if (event.isCancelled || mode == Mode.NONE) return
        val message = Censor.filterBadwords(event.message, mode)

        event.message = message
    }

    @EventHandler
    fun onPlayerJoin(event: PlayerLoginEvent) {
        val name = event.player.displayName
        val setMode: Mode = Mode.values().firstOrNull { mode -> event.player.hasPermission(mode.permission) && !Censor.GLOBAL_CENSOR }?.let { mode ->
            when (mode) {
                Mode.ADMIN -> mode
                Mode.MONITOR -> mode
                Mode.NONE -> mode
                Mode.FULL -> mode
                Mode.MILD -> mode
            }
        } ?: Censor.SERVER_MODE
        Censor.PLAYER_CACHE.putIfAbsent(name, setMode)
    }
}