package io.bitbucket.sg007.chatcensor

import io.bitbucket.sg007.chatcensor.utils.Censor
import net.md_5.bungee.api.ChatColor
import net.md_5.bungee.api.ChatColor.*
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.ComponentBuilder
import net.md_5.bungee.api.chat.HoverEvent
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class ChatBox {
    fun displayStatus(sender: CommandSender): TextComponent {
        val title = TextComponent("$BLACK--[")
        title.addExtra("$GRAY$BOLD${Main.PLUGIN_NAME} $ITALIC$DARK_GREEN${Main.VERSION}$RESET")
        title.addExtra("$BLACK]--")
        val display = TextComponent(title)
        display.addExtra("\n${GRAY}Enabled=${toggleCondition(Censor.ENABLED)}")
        if (sender is Player) {
            if (sender.hasPermission(Mode.ADMIN.permission)) {
                display.addExtra("\n${GRAY}Loaded Lists= ")
                Censor.LOADED_FILES.forEach { name, count ->
                    val fileTitle = TextComponent(toggleSwitchFormat(name, true))
                    fileTitle.hoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, ComponentBuilder("Words in file: $count").create())
                    display.addExtra(fileTitle)
                    display.addExtra(" ")
                }
            }
            var mode = sender.censor
            val options = mutableListOf<TextComponent>()
            val globalCensor = if (Censor.GLOBAL_CENSOR) "global" else "current"
            Mode.values().forEach { m ->
                if (Censor.GLOBAL_CENSOR) {
                    mode = Censor.SERVER_MODE
                    if (m == Mode.MONITOR || m == Mode.ADMIN) return@forEach
                }
                val clickMode = TextComponent(toggleSwitchFormat(m.name.toLowerCase(), mode == m))
                clickMode.hoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, ComponentBuilder("Click to change $globalCensor mode to ${ChatColor.BOLD}${m.name}").create())
                clickMode.clickEvent = ClickEvent(ClickEvent.Action.RUN_COMMAND, "/censor mode ${m.name}")
                options.add(clickMode)
            }
            val toggle = TextComponent(toggleCondition(Censor.GLOBAL_CENSOR))
            toggle.hoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, ComponentBuilder("Click to toggle global censor. Enforces censor for all players").create())
            toggle.clickEvent = ClickEvent(ClickEvent.Action.RUN_COMMAND, "/censor global")
            display.addExtra("\n${GRAY}Global Censor= ")
            display.addExtra(toggle)
            display.addExtra("\n$GRAY${if (Censor.GLOBAL_CENSOR) "Global Censor" else "Censor"} Mode= ")
            options.forEach { option ->
                display.addExtra(option)
                display.addExtra(" ")
            }
        }
        val footer = TextComponent("\n--------------")
        footer.color = BLACK
        display.addExtra(footer)
        return display
    }
}



