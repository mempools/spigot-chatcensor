package io.bitbucket.sg007.chatcensor

import io.bitbucket.sg007.chatcensor.utils.Censor
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler

class Commands(private val plugin: Main) : CommandExecutor {
    @EventHandler
    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        if (sender == null) return true
        if (args!!.isEmpty()) {
            sender.spigot().sendMessage(plugin.chatBox.displayStatus(sender))
            return true
        }
        val cmd: String = args[0]
        if (cmd.equals("help", ignoreCase = true)) return false
        when (cmd) {
            "info" -> {
                val results = TextComponent("${CHAT_TAG} Checking ${ChatColor.ITALIC}${Censor.PLAYER_CACHE.size} ${ChatColor.RESET}players' modes...")
                val search = if (args.size > 1) args[1] else ""
                if (search.isNotEmpty()) results.addExtra("\n${CHAT_TAG} Searching players' containing '${ChatColor.ITALIC}$search${ChatColor.RESET}'")
                var limit = 0
                Censor.PLAYER_CACHE.forEach { player, mode ->
                    if (limit == 10) return@forEach
                    if (player.toLowerCase().contains(search)) {
                        limit++
                        results.addExtra("\n ${ChatColor.YELLOW}$player ${ChatColor.GRAY}: mode=${ChatColor.YELLOW}${mode.name}")
                    }
                }
                sender.spigot().sendMessage(results)
                return true
            }
            "check" -> {
                val search = if (args.size > 1) args[1] else ""
                if (search.isEmpty()) {
                    sender.sendMessage("${CHAT_TAG} Enter a word to lookup")
                    return true
                }
                var limit = 0
                val results = TextComponent("${CHAT_TAG} Searching bad words containing '$search'...")
                Censor.BAD_WORDS.forEach { bw ->
                    if (limit == 15) return@forEach
                    if (bw.contains(search)) {
                        limit++
                        results.addExtra("\n$ ${ChatColor.RED}$bw")
                    }
                }
                sender.spigot().sendMessage(results)
                return true
            }
            "global" -> {
                if (sender.hasPermission(Mode.ADMIN.permission)) {
                    Censor.GLOBAL_CENSOR = !Censor.GLOBAL_CENSOR
                    sender.sendMessage("${CHAT_TAG} Global Censoring ${toggleCondition(Censor.GLOBAL_CENSOR)}")
                } else {
                    sender.sendMessage(Main.MESSAGES.noPermission)
                }
                return true
            }
            "mode" -> {
                if (Censor.GLOBAL_CENSOR && !sender.hasPermission(Mode.ADMIN.permission)) {
                    sender.sendMessage("${CHAT_TAG} Cannot switch modes when global censoring is enabled!")
                    return true
                }
                if (args.size < 2) {
                    sender.sendMessage("${CHAT_TAG} Enter a mode: none | mild | full | admin")
                    return true
                }
                var target: Player? = null
                if (args.size >= 3) {
                    val name = args[2]
                    target = Bukkit.getPlayer(name)
                }
                val input = args[1].toUpperCase()
                var newMode: Mode? = null
                Mode.values().forEach { mode -> if (mode.name.contains(input)) newMode = mode }
                if (newMode == null) {
                    sender.sendMessage("${CHAT_TAG} Invalid mode")
                    return true
                }

                //if player's mode if provided
                if (target != null && sender.hasPermission(Mode.ADMIN.permission)) {
                    target.censor = newMode!!
                    sender.sendMessage("${CHAT_TAG} ${ChatColor.GRAY}Target '${ChatColor.ITALIC}${target.name}${ChatColor.RESET}${ChatColor.GRAY}' mode changed to: ${ChatColor.ITALIC}$newMode")
                    return true
                }

                // if global censor
                if (Censor.GLOBAL_CENSOR && sender.hasPermission(Mode.ADMIN.permission)) {
                    Censor.SERVER_MODE = newMode!!
                    sender.sendMessage("${CHAT_TAG} Global Censor changed to ${newMode!!}")
                    return true
                }

                // if non op
                if (!sender.hasPermission(newMode!!.permission)) {
                    sender.sendMessage(Main.MESSAGES.noPermission)
                    return true
                } else if (sender is Player) {
                    sender.censor = newMode!!
                    sender.sendMessage("${CHAT_TAG} Censor mode has been changed to $newMode")
                }
                return true
            }
        }
        return false
    }
}