package io.bitbucket.sg007.chatcensor

import io.bitbucket.sg007.chatcensor.utils.Censor
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitScheduler
import java.io.File

class Main : JavaPlugin() {
    companion object {
        const val PLUGIN_NAME = "ChatCensor"
        const val VERSION = 1.0
        val MESSAGES: Messages = Messages()
    }

    lateinit var chatBox: ChatBox
    val scheduler: BukkitScheduler = Bukkit.getScheduler()

    override fun onEnable() {
        setupConfig()
        chatBox = ChatBox()
        getCommand("censor").executor = Commands(this)
        server.pluginManager.registerEvents(CensorListener(), this)
    }

    @Throws(Exception::class)
    private fun setupConfig() {
        if (!dataFolder.exists()) dataFolder.mkdirs()
        val file = File(dataFolder, "config.yml")
        logger.info("Loading configuration..")
        if (!file.exists()) {
            logger.info("Creating configuration file...")
            config.apply {
                set("files", listOf("custom", "languages/en"))
                set("messages.no-permission", MESSAGES.noPermission)
                set("settings.enabled", true)
                set("settings.censor", '*')
                set("settings.default-mode", Mode.MILD.name)
            }
            config.save(file)
            saveResource("custom", false)
            Censor.LANGUAGES.forEach { lang, _ ->
                saveResource("languages${File.separator}$lang", false)
            }
        } else {
            config.load(file)
        }
        MESSAGES.apply {
            noPermission = config.getString("messages.no-permission")
        }
        Censor.apply {
            loadDictionary(dataFolder, config.getStringList("files"))
            ENABLED = config.getBoolean("settings.enabled")
            SERVER_MODE = Mode.valueOf(config.getString("settings.default-mode"))
            MASK = config.getString("settings.censor")
        }
        logger.info("Loaded ${Censor.BAD_WORDS.size} censored words")
    }
}