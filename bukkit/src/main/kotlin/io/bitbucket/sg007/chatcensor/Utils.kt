package io.bitbucket.sg007.chatcensor

import io.bitbucket.sg007.chatcensor.utils.Censor
import net.md_5.bungee.api.ChatColor
import org.bukkit.entity.Player

data class Messages(var noPermission: String = "${CHAT_TAG} You do not have permission")

val CHAT_TAG = " ${ChatColor.DARK_GRAY}(${ChatColor.BOLD}${ChatColor.YELLOW}*${ChatColor.RESET}${ChatColor.DARK_GRAY})${ChatColor.RESET}"

var Player.censor
    get() = Censor.PLAYER_CACHE.getOrDefault(player.displayName, Censor.SERVER_MODE)
    set(new) {
        if (Censor.PLAYER_CACHE.containsKey(player.displayName)) Censor.PLAYER_CACHE[player.displayName] = new else
            Censor.PLAYER_CACHE.putIfAbsent(player.displayName, new)
    }

fun toggleCondition(condition: Boolean): String {
    return toggleSwitchFormat("$condition", condition)
}

fun toggleSwitchFormat(text: String, condition: Boolean): String {
    return toggleSwitchFormat(text, condition, "${ChatColor.GREEN}", "${ChatColor.RED}")
}

fun toggleSwitchFormat(text: String, condition: Boolean, enabledFormat: String, disabledFormat: String): String {
    return (if (condition) "$enabledFormat$text" else "$disabledFormat$text") + "${ChatColor.RESET}"
}