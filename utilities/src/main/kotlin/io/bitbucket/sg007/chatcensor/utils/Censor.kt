package io.bitbucket.sg007.chatcensor.utils

import io.bitbucket.sg007.mc.chatcensor.api.Mode
import java.io.File

class Censor {
    companion object {
        var ENABLED = true

        var GLOBAL_CENSOR = false
        var SERVER_MODE = Mode.FULL
        val PLAYER_CACHE = mutableMapOf<String, Mode>()
        val LOADED_FILES = mutableMapOf<String, Int>()
        var MASK = "*"
        val BAD_WORDS = mutableListOf<String>()
        val LANGUAGES = mutableMapOf(
                "ar" to false,
                "cs" to false,
                "da" to false,
                "de" to false,
                "en" to false,
                "eo" to false,
                "es" to false,
                "fa" to false,
                "fi" to false,
                "fr" to false,
                "hi" to false,
                "hu" to false,
                "it" to false,
                "ja" to false,
                "ko" to false,
                "nl" to false,
                "no" to false,
                "pl" to false,
                "pt" to false,
                "ru" to false,
                "sv" to false,
                "th" to false,
                "tlh" to false,
                "tr" to false,
                "zh" to false
        )
        var LETTER_PATTERNS = mutableMapOf(
                'a' to "[aA@]",
                'b' to "[bB]",
                'c' to "[cCkK]",
                'd' to "[dD]",
                'e' to "[eE3]",
                'f' to "[fF]",
                'g' to "[gG6]",
                'h' to "[hH]",
                'i' to "[iIl!1]",
                'j' to "[jJ]",
                'k' to "[cCkK]",
                'l' to "[lL1!i]",
                'm' to "[mM]",
                'n' to "[nN]",
                'o' to "[oO0]",
                'p' to "[pP]",
                'q' to "[qQ9]",
                'r' to "[rR]",
                's' to "[sS$5]",
                't' to "[tT7]",
                'u' to "[uUvV]",
                'v' to "[vVuU]",
                'w' to "[wW]",
                'x' to "[xX]",
                'y' to "[yY]",
                'z' to "[zZ2]"
        )

        fun censorText(text: String, mode: Mode): String {
            val char = text.toCharArray()
            when (mode) {
            //todo: add configurable substring number?     v ---- this one
                Mode.MILD -> for (i in 1 until char.size - 1) char[i] = MASK.toCharArray()[0]
                Mode.FULL -> for (i in 0 until char.size) char[i] = MASK.toCharArray()[0]
                else -> return String(char)
            }
            return String(char)
        }

        fun filterBadwords(text: String, mode: Mode): String {
            if (mode == Mode.NONE || mode == Mode.MONITOR || mode == Mode.ADMIN) return text
            var filteredText = text
            BAD_WORDS.forEach { badWord ->
                if (filteredText.contains(badWord, ignoreCase = true)) filteredText = filteredText.replace(badWord, censorText(badWord, mode), ignoreCase = true)
                val replacedWords = mutableListOf<String>()
                LETTER_PATTERNS.forEach { letter, pattern ->
                    pattern.forEach { char -> if (char != '[' || char != ']') replacedWords.add(badWord.replace(letter, char, ignoreCase = true)) }
                }
                replacedWords.forEach { replacement ->
                    if (filteredText.contains(replacement, ignoreCase = true)) filteredText = filteredText.replace(replacement, censorText(replacement, mode), ignoreCase = true)
                }
            }
            return filteredText
        }

        //todo command to view list of bad words and search bad words
        fun loadDictionary(directory: File, files: List<String>): List<String> {
            files.forEach { fileName ->
                var wordsInFile = 0
                val file = File(directory, fileName)
                if (!file.exists()) {
                    println("File: $file does not exist")
                    return@forEach
                }
                val reader = file.bufferedReader()
                reader.useLines { words ->
                    words.forEach { badWord ->
                        wordsInFile++
                        BAD_WORDS.add(badWord)
                    }
                }
                LOADED_FILES.put(file.name, wordsInFile)
            }
            return BAD_WORDS
        }
    }
}