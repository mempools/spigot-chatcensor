package io.bitbucket.sg007.mc.chatcensor.api

enum class Mode(val desc: String, val permission: String) {
    FULL("full", "chatcensor.full"),
    MILD("mild", "chatcensor.mild"),
    NONE("none", "chatcensor.none"),
    MONITOR("monitor", "chatcensor.monitor"),
    ADMIN("admin", "chatcensor.admin");
}