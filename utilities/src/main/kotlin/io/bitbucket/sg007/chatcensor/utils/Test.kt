package io.bitbucket.sg007.chatcensor.utils

import io.bitbucket.sg007.mc.chatcensor.api.Mode
import java.util.*

fun main(args: Array<String>) {
    println("Censor Test version: ")
    wordbank_en.split("\n").forEach { word -> Censor.BAD_WORDS.add(word) }
    println("Loaded ${Censor.BAD_WORDS.size} censored words")
    println("Commands:\n  example <text to censor>  - Displays example censors with debug information\n  check <word>  -  Checks if a word is a bad\n  quit, q  - Exit")
    println("------------------------ex---------------")
    val scanner = Scanner(System.`in`)
    loop@ while (true) {
        println("\nEnter command: ")
        val command = scanner.nextLine()
        val cargs = command.split(' ')
        when (cargs[0]) {
            "example" -> {
                val test = listOf(Mode.NONE, Mode.MILD, Mode.FULL)
                var testString = "Ass ASs AsS @ss"
                if (cargs.size > 1) {
                    testString = ""
                    cargs.subList(1, cargs.size).forEach { word -> testString += "$word " }
                }
                test.forEach { mode ->
                    println("Mode: $mode")
                    println(" Output: ${Censor.filterBadwords(testString, mode)}")
                }
            }
            "check" -> {
                if (cargs.size > 1) {
                    val search = cargs[1]
                    val bad = Censor.BAD_WORDS.contains(search)
                    println("Word: $search  Bad?: $bad")
                }
            }
            "quit", "q" -> break@loop
            else -> println("Unknown command")
        }
    }
}

private val wordbank_en = "2g1c\n" +
        "2 girls 1 cup\n" +
        "acrotomophilia\n" +
        "alabama hot pocket\n" +
        "alaskan pipeline\n" +
        "anal\n" +
        "anilingus\n" +
        "anus\n" +
        "apeshit\n" +
        "arsehole\n" +
        "ass\n" +
        "asshole\n" +
        "assmunch\n" +
        "auto erotic\n" +
        "autoerotic\n" +
        "babeland\n" +
        "baby batter\n" +
        "baby juice\n" +
        "ball gag\n" +
        "ball gravy\n" +
        "ball kicking\n" +
        "ball licking\n" +
        "ball sack\n" +
        "ball sucking\n" +
        "bangbros\n" +
        "bareback\n" +
        "barely legal\n" +
        "barenaked\n" +
        "bastard\n" +
        "bastardo\n" +
        "bastinado\n" +
        "bbw\n" +
        "bdsm\n" +
        "beaner\n" +
        "beaners\n" +
        "beaver cleaver\n" +
        "beaver lips\n" +
        "bestiality\n" +
        "big black\n" +
        "big breasts\n" +
        "big knockers\n" +
        "big tits\n" +
        "bimbos\n" +
        "birdlock\n" +
        "bitch\n" +
        "bitches\n" +
        "black cock\n" +
        "blonde action\n" +
        "blonde on blonde action\n" +
        "blowjob\n" +
        "blow job\n" +
        "blow your load\n" +
        "blue waffle\n" +
        "blumpkin\n" +
        "bollocks\n" +
        "bondage\n" +
        "boner\n" +
        "boob\n" +
        "boobs\n" +
        "booty call\n" +
        "brown showers\n" +
        "brunette action\n" +
        "bukkake\n" +
        "bulldyke\n" +
        "bullet vibe\n" +
        "bullshit\n" +
        "bung hole\n" +
        "bunghole\n" +
        "busty\n" +
        "butt\n" +
        "buttcheeks\n" +
        "butthole\n" +
        "camel toe\n" +
        "camgirl\n" +
        "camslut\n" +
        "camwhore\n" +
        "carpet muncher\n" +
        "carpetmuncher\n" +
        "chocolate rosebuds\n" +
        "circlejerk\n" +
        "cleveland steamer\n" +
        "clit\n" +
        "clitoris\n" +
        "clover clamps\n" +
        "clusterfuck\n" +
        "cock\n" +
        "cocks\n" +
        "coprolagnia\n" +
        "coprophilia\n" +
        "cornhole\n" +
        "coon\n" +
        "coons\n" +
        "creampie\n" +
        "cum\n" +
        "cumming\n" +
        "cunnilingus\n" +
        "cunt\n" +
        "darkie\n" +
        "date rape\n" +
        "daterape\n" +
        "deep throat\n" +
        "deepthroat\n" +
        "dendrophilia\n" +
        "dick\n" +
        "dildo\n" +
        "dingleberry\n" +
        "dingleberries\n" +
        "dirty pillows\n" +
        "dirty sanchez\n" +
        "doggie style\n" +
        "doggiestyle\n" +
        "doggy style\n" +
        "doggystyle\n" +
        "dog style\n" +
        "dolcett\n" +
        "domination\n" +
        "dominatrix\n" +
        "dommes\n" +
        "donkey punch\n" +
        "double dong\n" +
        "double penetration\n" +
        "dp action\n" +
        "dry hump\n" +
        "dvda\n" +
        "eat my ass\n" +
        "ecchi\n" +
        "ejaculation\n" +
        "erotic\n" +
        "erotism\n" +
        "escort\n" +
        "eunuch\n" +
        "fag\n" +
        "faggot\n" +
        "fecal\n" +
        "felch\n" +
        "fellatio\n" +
        "feltch\n" +
        "female squirting\n" +
        "femdom\n" +
        "figging\n" +
        "fingerbang\n" +
        "fingering\n" +
        "fisting\n" +
        "foot fetish\n" +
        "footjob\n" +
        "frotting\n" +
        "fuck\n" +
        "fuck buttons\n" +
        "fuckin\n" +
        "fucking\n" +
        "fucktards\n" +
        "fudge packer\n" +
        "fudgepacker\n" +
        "futanari\n" +
        "gang bang\n" +
        "gay sex\n" +
        "genitals\n" +
        "giant cock\n" +
        "girl on\n" +
        "girl on top\n" +
        "girls gone wild\n" +
        "goatcx\n" +
        "goatse\n" +
        "god damn\n" +
        "gokkun\n" +
        "golden shower\n" +
        "goodpoop\n" +
        "goo girl\n" +
        "goregasm\n" +
        "grope\n" +
        "group sex\n" +
        "g-spot\n" +
        "guro\n" +
        "hand job\n" +
        "handjob\n" +
        "hard core\n" +
        "hardcore\n" +
        "hentai\n" +
        "homoerotic\n" +
        "honkey\n" +
        "hooker\n" +
        "hot carl\n" +
        "hot chick\n" +
        "how to kill\n" +
        "how to murder\n" +
        "huge fat\n" +
        "humping\n" +
        "incest\n" +
        "intercourse\n" +
        "jack off\n" +
        "jail bait\n" +
        "jailbait\n" +
        "jelly donut\n" +
        "jerk off\n" +
        "jigaboo\n" +
        "jiggaboo\n" +
        "jiggerboo\n" +
        "jizz\n" +
        "juggs\n" +
        "kike\n" +
        "kinbaku\n" +
        "kinkster\n" +
        "kinky\n" +
        "knobbing\n" +
        "leather restraint\n" +
        "leather straight jacket\n" +
        "lemon party\n" +
        "lolita\n" +
        "lovemaking\n" +
        "make me come\n" +
        "male squirting\n" +
        "masturbate\n" +
        "menage a trois\n" +
        "milf\n" +
        "missionary position\n" +
        "motherfucker\n" +
        "mound of venus\n" +
        "mr hands\n" +
        "muff diver\n" +
        "muffdiving\n" +
        "nambla\n" +
        "nawashi\n" +
        "negro\n" +
        "neonazi\n" +
        "nigga\n" +
        "nigger\n" +
        "nig nog\n" +
        "nimphomania\n" +
        "nipple\n" +
        "nipples\n" +
        "nsfw images\n" +
        "nude\n" +
        "nudity\n" +
        "nympho\n" +
        "nymphomania\n" +
        "octopussy\n" +
        "omorashi\n" +
        "one cup two girls\n" +
        "one guy one jar\n" +
        "orgasm\n" +
        "orgy\n" +
        "paedophile\n" +
        "paki\n" +
        "panties\n" +
        "panty\n" +
        "pedobear\n" +
        "pedophile\n" +
        "pegging\n" +
        "penis\n" +
        "phone sex\n" +
        "piece of shit\n" +
        "pissing\n" +
        "piss pig\n" +
        "pisspig\n" +
        "playboy\n" +
        "pleasure chest\n" +
        "pole smoker\n" +
        "ponyplay\n" +
        "poof\n" +
        "poon\n" +
        "poontang\n" +
        "punany\n" +
        "poop chute\n" +
        "poopchute\n" +
        "porn\n" +
        "porno\n" +
        "pornography\n" +
        "prince albert piercing\n" +
        "pthc\n" +
        "pubes\n" +
        "pussy\n" +
        "queaf\n" +
        "queef\n" +
        "quim\n" +
        "raghead\n" +
        "raging boner\n" +
        "rape\n" +
        "raping\n" +
        "rapist\n" +
        "rectum\n" +
        "reverse cowgirl\n" +
        "rimjob\n" +
        "rimming\n" +
        "rosy palm\n" +
        "rosy palm and her 5 sisters\n" +
        "rusty trombone\n" +
        "sadism\n" +
        "santorum\n" +
        "scat\n" +
        "schlong\n" +
        "scissoring\n" +
        "semen\n" +
        "sex\n" +
        "sexo\n" +
        "sexy\n" +
        "shaved beaver\n" +
        "shaved pussy\n" +
        "shemale\n" +
        "shibari\n" +
        "shit\n" +
        "shitblimp\n" +
        "shitty\n" +
        "shota\n" +
        "shrimping\n" +
        "skeet\n" +
        "slanteye\n" +
        "slut\n" +
        "s&m\n" +
        "smut\n" +
        "snatch\n" +
        "snowballing\n" +
        "sodomize\n" +
        "sodomy\n" +
        "spic\n" +
        "splooge\n" +
        "splooge moose\n" +
        "spooge\n" +
        "spread legs\n" +
        "spunk\n" +
        "strap on\n" +
        "strapon\n" +
        "strappado\n" +
        "strip club\n" +
        "style doggy\n" +
        "suck\n" +
        "sucks\n" +
        "suicide girls\n" +
        "sultry women\n" +
        "swastika\n" +
        "swinger\n" +
        "tainted love\n" +
        "taste my\n" +
        "tea bagging\n" +
        "threesome\n" +
        "throating\n" +
        "tied up\n" +
        "tight white\n" +
        "tit\n" +
        "tits\n" +
        "titties\n" +
        "titty\n" +
        "tongue in a\n" +
        "topless\n" +
        "tosser\n" +
        "towelhead\n" +
        "tranny\n" +
        "tribadism\n" +
        "tub girl\n" +
        "tubgirl\n" +
        "tushy\n" +
        "twat\n" +
        "twink\n" +
        "twinkie\n" +
        "two girls one cup\n" +
        "undressing\n" +
        "upskirt\n" +
        "urethra play\n" +
        "urophilia\n" +
        "vagina\n" +
        "venus mound\n" +
        "vibrator\n" +
        "violet wand\n" +
        "vorarephilia\n" +
        "voyeur\n" +
        "vulva\n" +
        "wank\n" +
        "wetback\n" +
        "wet dream\n" +
        "white power\n" +
        "wrapping men\n" +
        "wrinkled starfish\n" +
        "xx\n" +
        "xxx\n" +
        "yaoi\n" +
        "yellow showers\n" +
        "yiffy\n" +
        "zoophilia\n" +
        "\uD83D\uDD95"
